import { createRouter, createWebHistory } from "vue-router";
import HomeView from "../views/HomeView.vue";

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: "/",
      name: "home",
      component: HomeView,
    },
    {
      path: "/about",
      name: "about",
      // route level code-splitting
      // this generates a separate chunk (About.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      component: () => import("../views/AboutView.vue"),
    },

    {
      path: "/my-files",
      name: "MyFiles",
      component: () => import("../views/myFiles.vue"),
    },

    {
      path: "/my-profile",
      name: "my-profile",
      component: () => import("../views/myProFile.vue"),
    },

    {
      path: "/star-xo",
      name: "StarXO",
      component: () => import("../views/StarSo.vue"),
    },
  ],
});

export default router;
